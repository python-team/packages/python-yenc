python-yenc (0.4.0-11) UNRELEASED; urgency=medium

  * Control: remove team from uploaders.

 -- Jeroen Ploemen <jcfp@debian.org>  Tue, 19 Mar 2024 07:04:48 +0000

python-yenc (0.4.0-10) unstable; urgency=medium

  * Patches: add 03 to define PY_SSIZE_T_CLEAN, fixing test failures
    with Python 3.10. (Closes: #999394)

 -- Jeroen Ploemen <jcfp@debian.org>  Fri, 12 Nov 2021 09:30:01 +0000

python-yenc (0.4.0-9) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Jeroen Ploemen ]
  * Control:
    + set Rules-Requires-Root: no.
    + set maintainer to my debian.org email address.
    + bump compat to 13 (from 12), Standards-Version to 4.6.0 (from
      4.4.1; no further changes).
  * Copyright:
    + use my debian.org email address.
    + bump years for packaging.
  * Watch: bump version to 4 (from 3; no further changes).
  * Rules: write out 'buildsystem', abbreviations are no longer
    accepted by dh.

 -- Jeroen Ploemen <jcfp@debian.org>  Sat, 23 Oct 2021 08:25:53 +0000

python-yenc (0.4.0-8) unstable; urgency=medium

  * Source-only upload.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Thu, 02 Jan 2020 14:50:54 +0000

python-yenc (0.4.0-7) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ JCF Ploemen (jcfp) ]
  * Reintroduce the package after it was accidentally removed from
    the archive.
  * Control: remove useless ${python3:Provides}.
  * Tests: fix upstream-testsuite for multiple supported Python3
    versions.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Fri, 29 Nov 2019 09:25:56 +0000

python-yenc (0.4.0-6) unstable; urgency=medium

  * Team upload
  * Source-only upload

 -- Jonathan Carter <jcc@debian.org>  Wed, 18 Sep 2019 12:59:24 +0000

python-yenc (0.4.0-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ JCF Ploemen (jcfp) ]
  * Rules:
    + enable all hardening (i.e. +pie).
    + remove dh_compress override: examples are left uncompressed by
      default with compat 12.
  * Remove support for Python2.
  * Compat: bump level to 12 (from 11).
  * Bump Standards-Version to 4.4.0 (from 4.2.1; no further changes).
  * Tests: add upstream testsuite as autopkgtest, replacing the trivial
    autopkgtest-pkg-python.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Wed, 04 Sep 2019 18:50:24 +0000

python-yenc (0.4.0-4) unstable; urgency=medium

  * Control: add breaks/replaces to docs package to prevent file
    overwrite issues with incomplete upgrades. (Closes: #914683)
  * Rules:
    + remove dh_installchangelogs override, correct file is found
      automatically nowadays.
    + exclude examples from compression.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Sat, 01 Dec 2018 15:40:07 +0000

python-yenc (0.4.0-3) unstable; urgency=medium

  [ JCF Ploemen (jcfp) ]
  * Watch: switch upstream url to https.
  * Control:
    + add Testsuite: autopkgtest-pkg-python.
    + add python3-yenc binary package.
    + add python-yenc-doc package for docs and examples.
    + sort and reorder build-deps.
    + build-depend on python3-all-dev (Python 3 support).
    + use https for upstream homepage.
    + bump standards-version to 4.2.1 (from 3.9.8).
  * Compat: bump to 11 (from 9).
  * Rules:
    + add python3 addon to dh sequencer command and set buildsys to
      pybuild.
    + override dh_auto_test to use custom upstream test system.
  * Patches: add 02 to add support for Python 3, based on a pull request
    at upstream bitbucket.
  * Copyright:
    + use https for upstream source location.
    + add author that added the python3 support as a copyright holder.
    + bump years for packaging.
  * d/clean: add to clean leftover cruft.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Mon, 29 Oct 2018 16:34:50 +0000

python-yenc (0.4.0-2) unstable; urgency=low

  [ JCF Ploemen (jcfp) ]
  * debian/rules:
    + install upstream changelog.
    + enable all hardening (except pie, ftbfs).
  * Add build-dep on dh-python, now a separate package.
  * Convert copyright to machine-readable format.
  * Bump Standards-Version to 3.9.8 (from 3.9.4, no further changes).

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Sat, 07 Jan 2017 14:12:09 +0000

python-yenc (0.4.0-1) unstable; urgency=low

  * New upstream release (Closes: #470712).
  * New maintainer (Closes: #673948).
  * Update watch file.
  * Update copyright, license changed to LGPL.
  * debian/rules:
    + replace entirely with dh sequencer.
    + switch to dh_python2.
  * Switch to source format 3.0 (quilt) (Closes: #664294).
  * Bump Standards-Version to 3.9.4.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Wed, 29 May 2013 14:25:56 +0200

python-yenc (0.3+debian-2) unstable; urgency=low

  * Add debian/watch.
  * Move to section python.
  * Move homepage to new dpkg standard field.
  * Bump Standards-Version: to 3.7.3.

 -- Adam Cécile (Le_Vert) <gandalf@le-vert.net>  Mon, 28 Jan 2008 20:45:23 +0100

python-yenc (0.3+debian-1) unstable; urgency=low

  * Initial release (Closes: #418745).

 -- Adam Cécile (Le_Vert) <gandalf@le-vert.net>  Wed, 11 Apr 2007 17:04:29 +0200
